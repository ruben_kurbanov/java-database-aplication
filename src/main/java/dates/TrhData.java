package dates;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TrhData {

    private final SimpleIntegerProperty id_new_trh;
    private final SimpleStringProperty name_new_trh;
    private final SimpleStringProperty phone_new_trh;
    private final SimpleIntegerProperty psc_new_trh;
    private final SimpleStringProperty city_new_trh;


    public TrhData(int id_new_trh, String name_new_trh,
                   String phone_new_trh, int psc_new_trh,
                   String city_new_trh) {
        this.id_new_trh = new SimpleIntegerProperty(id_new_trh);
        this.name_new_trh = new SimpleStringProperty(name_new_trh);
        this.phone_new_trh = new SimpleStringProperty(phone_new_trh);
        this.psc_new_trh = new SimpleIntegerProperty(psc_new_trh);
        this.city_new_trh = new SimpleStringProperty(city_new_trh);

    }

    public int getId_new_trh() {
        return id_new_trh.get();
    }

    public IntegerProperty id_new_trhProperty() {
        return id_new_trh;
    }

    public void setId_new_trh(int id_new_trh) {
        this.id_new_trh.set(id_new_trh);
    }

    public String getName_new_trh() {
        return name_new_trh.get();
    }

    public StringProperty name_new_trhProperty() {
        return name_new_trh;
    }

    public void setName_new_trh(String name_new_trh) {
        this.name_new_trh.set(name_new_trh);
    }

    public String getPhone_new_trh() {
        return phone_new_trh.get();
    }

    public StringProperty phone_new_trhProperty() {
        return phone_new_trh;
    }

    public void setPhone_new_trh(String phone_new_trh) {
        this.phone_new_trh.set(phone_new_trh);
    }

    public int getPsc_new_trh() {
        return psc_new_trh.get();
    }

    public IntegerProperty psc_new_trhProperty() {
        return psc_new_trh;
    }

    public void setPsc_new_trh(int psc_new_trh) {
        this.psc_new_trh.set(psc_new_trh);
    }

    public String getCity_new_trh() {
        return city_new_trh.get();
    }

    public StringProperty city_new_trhProperty() {
        return city_new_trh;
    }

    public void setCity_new_trh(String city_new_trh) {
        this.city_new_trh.set(city_new_trh);
    }

}
