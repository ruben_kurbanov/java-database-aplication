package model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "farmarz")
public class Farmar {
    @Id
    private int id_farmar;

    @Column(name = "email")
    private String email;

    @Column(name = "mesto")
    private String mesto;

    @Column(name = "nazev")
    private String name;

    @Column(name = "telefon")
    private String telefon;

    @ManyToMany(mappedBy = "farmari")
    private List<Trh> trhs;

    public Farmar(){

    }

    public Farmar(int id_farmar, String email, String mesto, String name, String telefon) {
        this.email = email;
        this.mesto = mesto;
        this.name = name;
        this.telefon = telefon;
        this.id_farmar = id_farmar;
    }

    public int getId_farmar() {
        return id_farmar;
    }

    public void setId_farmar(int id_farmar) {
        this.id_farmar = id_farmar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public List<Trh> getTrhs() {
        return trhs;
    }

    public void setTrhs(List<Trh> trhs) {
        this.trhs = trhs;
    }

}
